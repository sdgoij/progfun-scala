package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    def f(row: Int, col: Int, t: Int): Int =
      if (col == 0 || col == row) t else
        if (col > row || col < 0 || row < 0) 0 else
          f(row-1, col-1, t+f(row-1, col, 1))
    f(r, c, 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def f(ch: List[Char], s: List[Char]): Boolean = {
      if (ch.isEmpty) s.isEmpty else {
        ch.head match {
          case '(' => f(ch.tail, ch.head::s)
          case ')' =>
            if (!s.isEmpty && s.head == '(')
              f(ch.tail, s.tail)
            else false
          case _ => f(ch.tail, s)
        }
      }
    }
    f(chars, List())
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def change(a: Int, n: List[Int], c: Int): Int =
      if (a == 0) c+1 else if (n.isEmpty || a < 0) c else
        change(a, n.tail, c + change(a - n.head, n, 0))
    change(money, coins.sortWith((a, b) => (a < b)), 0)
  }
}
