package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("combine singleton or nil") {
    val leafs = List(Leaf('e', 1), Fork(Leaf('x', 2), Leaf('w', 3), List('x', 'w'), 5))
    assert(combine(leafs) === List(Fork(Leaf('e',1), Fork(Leaf('x', 2), Leaf('w', 3), List('x', 'w'), 5), List('e', 'x', 'w'), 6)))
    assert(combine(List(Leaf('a', 2))) === List(Leaf('a', 2)))
    assert(combine(Nil) === List())
  }

  test("make tree") {
    new TestTrees {
      assert(createCodeTree("aabbb".toList) === t1)
      assert(createCodeTree("abbba".toList) === t1)
      assert(createCodeTree("babab".toList) === t1)
      val tree = createCodeTree("aabbbdddd".toList)
      assert(createCodeTree("dabbdabdd".toList) === tree)
      assert(chars(tree) === List('d', 'a', 'b'))
      assert(weight(tree) === 9)
    }
    assert(weight(createCodeTree("this is an example of a huffman tree".toList)) === 36)
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("times") {
    val m = times("Hello World!".toList)
    assert(!m.find(x => (x._1, x._2) == ('H', 1)).isEmpty)
    assert(!m.find(x => (x._1, x._2) == ('l', 3)).isEmpty)
    assert(!m.find(x => (x._1, x._2) == ('o', 2)).isEmpty)
  }

  test("make ordered leaf list") {
    val ol = makeOrderedLeafList(times("Hello World!".toList))
    assert(ol.head === Leaf('!', 1))
    assert(ol.last === Leaf('l', 3))
  }

  test("singleton") {
    val t0 = List()
    val t1 = Leaf('a', 1)::t0
    val t2 = Leaf('b', 1)::t1
    assert(!singleton(t0))
    assert(!singleton(t2))
    assert(singleton(t1))
  }

  test("until") {
    assert(until(singleton, combine)(List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 3))) === 
      List(Fork(Fork(Leaf('e', 1), Leaf('t' , 2), List('e', 't'), 3), Leaf('x', 3), List('e', 't', 'x'), 6)))
  }

  test("decoded secret") {
    assert(decodedSecret === List('h', 'u', 'f', 'f', 'm', 'a', 'n', 'e', 's', 't', 'c', 'o', 'o', 'l'))
  }

  test("encode secret") {
    assert(encode(frenchCode)("huffmanestcool".toList) === secret)
  }

  test("codebits") {
    val t: CodeTable = List(('a', List(0,1,1,0)), ('b', List(1,0,1,0)))
    assert(codeBits(t)('a') === List(0,1,1,0))
    assert(codeBits(t)('b') === List(1,0,1,0))
    assert(codeBits(t)('c') === List())
  }

  test("convert tree to table") {
    new TestTrees {
      val table = convert(t1)
      assert(codeBits(table)('a') === List(0))
      assert(codeBits(table)('b') === List(1))
    }
  }

  test("merge tables") {
    val fn = codeBits(mergeCodeTables(List(('a', List(0,0))), List(('b', List(0,1)))))_
    assert(fn('a') === List(0,0))
    assert(fn('b') === List(0,1))
    assert(fn('c') === List())
  }

  test("quick encode") {
    new TestTrees {
      assert(decode(t1, quickEncode(t1)("ab".toList)) === "ab".toList)
    }
    val text = "huffmanestcool".toList
    assert(decode(frenchCode, quickEncode(frenchCode)(text)) === text)
    assert(quickEncode(frenchCode)(text) === secret)
  }
}
