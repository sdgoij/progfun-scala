package example

object Main extends App {
  println(Lists.sum(List(1, 2, 3)))
  println(Lists.max(List(1, 2, 3)))
  try {
    println("Finding the max element in empty list, throws 'java.util.NoSuchElementException'")
    println(Lists.max((List())))
  } catch {
    case _: java.util.NoSuchElementException => println("Caught expected exception") 
    case _: Exception => println("Oops unexpected exception type!")
  }
}
