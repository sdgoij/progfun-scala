package objsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TweetSetSuite extends FunSuite {
  trait TestSets {
    val set1 = new Empty
    val set2 = set1.incl(new Tweet("a", "a body", 20))
    val set3 = set2.incl(new Tweet("b", "b body", 20))
    val c = new Tweet("c", "c body", 7)
    val d = new Tweet("d", "d body", 9)
    val set4c = set3.incl(c)
    val set4d = set3.incl(d)
    val set5 = set4c.incl(d)
  }

  def size(set: TweetSet): Int = {
    if (set.isEmpty) 0
    else 1 + size(set.tail)
  }

  test("filter: on empty set") {
    new TestSets {
      assert(size(set1.filter(tw => tw.user == "a")) === 0)
    }
  }

  test("filter: a on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.user == "a")) === 1)
    }
  }

  test("filter: 20 on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.retweets == 20)) === 2)
    }
  }

  test("union: set4c and set4d") {
    new TestSets {
      assert(size(set4c.union(set4d)) === 4)
    }
  }

  test("union: with empty set (1)") {
    new TestSets {
      assert(size(set5.union(set1)) === 4)
    }
  }

  test("union: with empty set (2)") {
    new TestSets {
      assert(size(set1.union(set5)) === 4)
    }
  }

  test("union: making sets bigger") {
    new TestSets {
      val u1 = set2.incl(new Tweet("x", "xzy", 1)).incl(new Tweet("y", "yzx", 2))
      val u2 = set5.incl(new Tweet("z", "zxy", 1)).incl(new Tweet("q", "qrs", 5))
      assert(size(u1.union(u2)) === 8, "u1 + u2")
    }
  }

  test("ascending: set5") {
    new TestSets {
      val trends = set5.ascendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user === "c")
    }
  }

  test("Google vs Apple") {
    def tr(t: Trending, a: Int): Int = if (t.isEmpty) a else tr(t.tail, a+1)
    assert(size(GoogleVsApple.googleTweets) === 38, "Google Tweets")
    assert(size(GoogleVsApple.appleTweets) === 150, "Apple Tweets")
    assert(tr(GoogleVsApple.trending, 0) === 179, "Trending Tweets")
  }
}
