package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PascalSuite extends FunSuite {
  import Main.pascal
  test("col=0,row=2") {
    assert(pascal(0,2) === 1)
  }

  test("col=1,row=2") {
    assert(pascal(1,2) === 2)
  }

  test("col=1,row=3") {
    assert(pascal(1,3) === 3)
  }

  test("col=0,row=0") {
    assert(pascal(0,0) === 1)
  }

  test("col=1,row=0") {
    assert(pascal(1,0) === 0)
  }

  test("col=2,row=-1") {
    assert(pascal(2,-1) === 0)
  }

  test("col=-1,row=0") {
    assert(pascal(-1,0) === 0)
  }

  test("col=5,row=3") {
    assert(pascal(5,3) === 0)
  }

  test("col=-1,row=2") {
    assert(pascal(-1,2) === 0)
  }

  test("col=0,row=34") {
    assert(pascal(0,34) === 1)
  }

  test("col=34,row=34") {
    assert(pascal(34,34) === 1)
  }

  test("col=33,row=34") {
    assert(pascal(33,34) === 34)
  }

  test("col=13,row=35") {
    assert(pascal(13,35) === 1476337800)
  }
}
